import 'package:lab1_1/lab1_1.dart' as lab1_1;
import 'dart:io';

void main() {
  print(" MENU");
  print(" Select the choice you want to perform :");
  print(" 1. ADD");
  print(" 2. SUBTRACT");
  print(" 3. MULTIPLY");
  print(" 4. DIVIDE");
  print(" 5. EXIT");
  print(" Choice you want to enter : ");
  String? choice = stdin.readLineSync();
  switch (choice) {
    case "1":
      {
        print(" Enter the value of x : ");
        int? x = int.parse(stdin.readLineSync()!);
        print(" Enter the value of y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int add = x + y;
        print(" Sum of the two number is : ");
        print(add);
      }
      break;
    case "2":
      {
        print(" Enter the value of x : ");
        int? x = int.parse(stdin.readLineSync()!);
        print(" Enter the value of y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int sub = x - y;
        print(" Sum of the two number is : ");
        print(sub);
      }
      break;
    case "3":
      {
        print(" Enter the value of x : ");
        int? x = int.parse(stdin.readLineSync()!);
        print(" Enter the value of y : ");
        int? y = int.parse(stdin.readLineSync()!);
        int mul = x * y;
        print(" Sum of the two number is : ");
        print(mul);
      }
      break;
    case "4":
      {
        print(" Enter the value of x : ");
        int? x = int.parse(stdin.readLineSync()!);
        print(" Enter the value of y : ");
        int? y = int.parse(stdin.readLineSync()!);
        double div = (x / y);
        print(" Sum of the two number is : ");
        print(div);
      }
      break;
    case "5":
      break;
  }
}

